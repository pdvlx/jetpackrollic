﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;

public class SwipeMovement : MonoBehaviour
{
    public float LerpSpeed;
    private GameObject _player;
    private bool _slideLeft, _slideRight;//touchscreen's 'where to swipe' data.
    public GameObject middlePos, rightPos, leftPos;
    public Animator witchfly;
    private bool _onRight, _onLeft, _onMiddle;//where the player is going. ===Player is in the middle at first===
    


    private bool inCoroutine,inCoroutineDelay , readyToMove;
    
    void Start()
    {
        _onMiddle = true;
        
    }

    void Update()
    {
        _player = this.gameObject;
        _slideLeft = TouchScreen.swipeLeft;
        _slideRight = TouchScreen.swipeRight;
        if (TouchScreen.swipeLeft)
        {
            witchfly.SetTrigger("SwipeLeft");
        }
        if (TouchScreen.swipeRight)
        {
            witchfly.SetTrigger("SwipeRight");
        }
        //eğer tuşlara basmıyorsa bu ife giricek ve uçuş animasyonuna geçicek
        if (!TouchScreen.swipeRight && !TouchScreen.swipeLeft)
        {
            witchfly.ResetTrigger("SwipeRight");
            witchfly.ResetTrigger("SwipeLeft");
            witchfly.SetBool("RightFly", false);
            witchfly.SetBool("LeftFly", false);
        }
        whereToSlide();
        LerpMovePlayer();
    }
    
    void LerpMovePlayer()
    {
        if (_onLeft)
        {
            transform.position = Vector3.Lerp(this.transform.position, leftPos.transform.position, LerpSpeed*Time.deltaTime);
           // if (transform.position.x < leftPos.transform.position.x + 0.5f)
               // witchfly.SetBool("LeftFly",false);
        }
        if (_onRight)
            transform.position = Vector3.Lerp(this.transform.position, rightPos.transform.position, LerpSpeed*Time.deltaTime);
        if (_onMiddle)
            transform.position = Vector3.Lerp(this.transform.position, middlePos.transform.position, LerpSpeed*Time.deltaTime);
        
    }

    private IEnumerator leaningPlayer()
    {
        inCoroutine = true;
        
        yield return new WaitForSeconds(0.2f);
        if (witchfly.GetBool("LeftFly"))
            witchfly.SetBool("LeftFly", false);
        else if (witchfly.GetBool("RightFly"))
            witchfly.SetBool("RightFly", false);
        inCoroutine = false;
    }
   
    void whereToSlide()
    {
        if (_slideLeft)
        {
            if (_onMiddle)
            {
                _onLeft = true;
                _onRight = false;
                _onMiddle = false;
            }else if (_onRight)
            {
                _onLeft = false;
                _onRight = false;
                _onMiddle = true;
            }else if (_onLeft)
            {
                _onRight = false;
                _onMiddle = false;
                return;
            }
            witchfly.SetBool("LeftFly",true);
            if (!inCoroutine)
                StartCoroutine(leaningPlayer());


        }else if (_slideRight)
        {
            if (_onMiddle)
            {
                _onLeft = false;
                _onRight = true;
                _onMiddle = false;
            }else if (_onLeft)
            {
                _onLeft = false;
                _onRight = false;
                _onMiddle = true;
            }else if (_onRight)
            {
                _onLeft = false;
                _onMiddle = false;
                return;
            }
            witchfly.SetBool("RightFly",true);
            if (!inCoroutine)
                StartCoroutine(leaningPlayer());

        }
    }
}
