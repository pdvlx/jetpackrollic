﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRandom : MonoBehaviour
{
    //Seagul obstacle
    public GameObject leftObstacle;
    public GameObject midObstacle;
    public GameObject rightObstacle;
    //Gold
    public GameObject leftGold;
    public GameObject midGold;
    public GameObject rightGold;
    //
    public int number;
    public int obstacleTypeSelector;

    void Start()
    {
        obstacleTypeSelector = Random.Range(1, 2);
        if (obstacleTypeSelector == 0)
        {
            number = Random.Range(0, 6);
            RandomObstacle(number);
        }
        if (obstacleTypeSelector == 1)
        {
            number = Random.Range(0, 3);
            RandomGold(number);
        }
    }

    void RandomGold(int random)
    {
        rightObstacle.SetActive(false);
        midObstacle.SetActive(false);
        leftObstacle.SetActive(false);
        if (random == 0)
        {
            //Debug.Log("0 " + this);
            rightGold.SetActive(true);
            midGold.SetActive(false);
            leftGold.SetActive(false);
        }
        else if (random == 1)
        {
            //Debug.Log("1 " + this);
            rightGold.SetActive(false);
            midGold.SetActive(true);
            leftGold.SetActive(false);
        }
        else if (random == 2)
        {
            //Debug.Log("2 " + this);
            rightGold.SetActive(false);
            midGold.SetActive(false);
            leftGold.SetActive(true);
        }
    }

    //yan yana olan 3 engelden random olarak geçilecek yolu ayarlıyoruz.
    //bu random yöntemi değişecek daha iyi bir random algoritması eklenecek
    void RandomObstacle(int random)
    {
        rightGold.SetActive(false);
        midGold.SetActive(false);
        leftGold.SetActive(false);
        if (random == 0)
        {
            //Debug.Log("0 " + this);
            rightObstacle.SetActive(true);
            midObstacle.SetActive(true);
            leftObstacle.SetActive(false);
        }
        else if (random == 1)
        {
            //Debug.Log("1 " + this);
            rightObstacle.SetActive(false);
            midObstacle.SetActive(true);
            leftObstacle.SetActive(true);
        }
        else if (random == 2)
        {
            //Debug.Log("2 " + this);
            rightObstacle.SetActive(true);
            midObstacle.SetActive(false);
            leftObstacle.SetActive(true);
        }
        else if (random == 3)
        {
            //Debug.Log("3 " + this);
            leftObstacle.SetActive(false);
            rightObstacle.SetActive(false);
            midObstacle.SetActive(true);
        }
        else if (random == 4)
        {
            //Debug.Log("4 " + this);
            leftObstacle.SetActive(false);
            midObstacle.SetActive(false);
            rightObstacle.SetActive(true);
        }
        else if (random == 5)
        {
            //Debug.Log("5 " + this);
            rightObstacle.SetActive(false);
            midObstacle.SetActive(false);
            leftObstacle.SetActive(true);
        }
    }
}
