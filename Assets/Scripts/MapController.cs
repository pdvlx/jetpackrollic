﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;

public class MapController : MonoBehaviour
{
    public Transform playerTransform;
    public Transform mapTransform1;
    public GameObject mapGo2;
    public float mapDistance;
    private float distanceBetweenPlayerandMap;
    private bool distanceCheck;
    
    void Start()
    {
        
    }

    void Update()
    {
        distanceBetweenPlayerandMap = playerTransform.position.z - mapTransform1.position.z;
        if (distanceBetweenPlayerandMap > mapDistance/2 && !distanceCheck)
        {
            mapGo2 = Instantiate(mapTransform1.gameObject,new UnityEngine.Vector3(mapTransform1.position.x,mapTransform1.position.y,(mapTransform1.position.z+mapDistance*2)),Quaternion.identity);
            distanceCheck = true;
        }

        if (distanceBetweenPlayerandMap > mapDistance*2)
        {
            Destroy(mapTransform1.gameObject);
            mapTransform1 = mapGo2.transform;
            mapGo2 = null;
            distanceCheck = false;
        }
    }
}
