﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float flySpeed = 40f;
    private float tempSpeed;
    public int ratio,score;
    public Animator witchAnim;
    public Animator broomAnim;
    public ParticleSystem particle,smokeParticle;
    public AudioSource audioParticle;
    public MeshCollider floorMeshCollider;
    private Rigidbody playerRB;
    private bool isFlying;
    private bool isPressed;
    
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        isFlying = false;
        tempSpeed = 2f * flySpeed;
    }

    void Update()
    {
        //bastığımızda uçabiliyor işte napcan
        if (Input.GetMouseButtonDown(0) && !isFlying && !isPressed)
        {
            playerRB.velocity = (Vector3.up + Vector3.forward)*flySpeed;
            particle.gameObject.SetActive(true);
            audioParticle.Play();
        }
        //uçma konumuna geldikten sonra ileriye gidebilmesi için
        if (isFlying)
        {
            playerRB.velocity = tempSpeed*Vector3.forward;
            particle.gameObject.SetActive(true);
        }

        if (playerRB.velocity.magnitude > 10)
        {
            isPressed = true;
        }

        if (playerRB.velocity.magnitude < 0.2f)
        {
            smokeParticle.gameObject.SetActive(false);
        }
    }

/*    //indukatorden oyuncunun bastığı değer ile uçma potansiyelini değiştiriyoruz.
    public float InducatorSpeed()
    {
        float tempSpeed = flySpeed;
        int tempRatio = Mathf.Abs(ratio);
        if (tempRatio <= 1)
        {
            tempSpeed *= 2;
            Debug.Log(tempSpeed + " speed");
        }
        else if (tempRatio <=5)
        {
            tempSpeed *= 1.5f;
            Debug.Log(tempSpeed+ " speed");
        }
        else if (tempRatio <= 8)
        {
            Debug.Log(tempSpeed+ " speed");
            tempSpeed *= 1;
        }
        else
        {
            Debug.Log(tempSpeed+ " speed");
            tempSpeed = 0;
        }

        return tempSpeed;
    }*/

    private void OnCollisionEnter(Collision other)
    {
        //havadaki uçabildiği katmana geldiğini kontrol ediyor

        if (other.gameObject.CompareTag("FirstFloor"))
        {
            isFlying = true;
            audioParticle.Play();
            //witchAnim.SetBool("StraightFly" , true);
            //broomAnim.SetBool("BroomStraight" , true);
        }
        if (other.gameObject.CompareTag("Ground") && isPressed)
        {
            Debug.Log("anim end");
            witchAnim.SetBool("DownEnding",true);
            broomAnim.SetBool("BroomDownEnding",true);
            smokeParticle.gameObject.SetActive(true);
        }

    }

    
    private void OnCollisionExit(Collision other)
    {
        //havadaki yolu bittiğinde düşme ile alakalı kontrolcüler burada değişecek
        if (other.gameObject.CompareTag("FirstFloor"))
        {
            isFlying = false;
            witchAnim.SetBool("DownStart",true); 
            broomAnim.SetBool("BroomDown",true);
            particle.gameObject.SetActive(false);
            audioParticle.Stop();
            //playerRB.velocity = Vector3.forward*flySpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            floorMeshCollider.isTrigger = true;
        }if (other.gameObject.CompareTag("Gold"))
        {
            score += 5;
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //alttan geçtikten sonra katmandaki triggerı kaldırıp collider ile çalışmasını sağlıyoruz.
        if (other.gameObject.CompareTag("FirstFloor"))
        {
            floorMeshCollider = other.gameObject.GetComponent<MeshCollider>();
            floorMeshCollider.isTrigger = false;
            witchAnim.SetBool("MidAir", false);
            broomAnim.SetBool("BroomMidAir", false);
            audioParticle.Stop();
            particle.gameObject.SetActive(false);
        }

    }
}
