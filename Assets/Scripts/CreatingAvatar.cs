﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CreatingAvatar : MonoBehaviour
{
    void Start()
    {
        GameObject activeGameObject = Selection.activeGameObject;

        if (activeGameObject != null &&
            activeGameObject.GetComponent<Animator>() != null)
        {
            Avatar avatar = AvatarBuilder.BuildGenericAvatar(activeGameObject, "");
            avatar.name = "Witch";
            Debug.Log(avatar.isHuman ? "is human" : "is generic");

            Animator animator = activeGameObject.GetComponent<Animator>() as Animator;
            animator.avatar = avatar;
        }
    }
}
