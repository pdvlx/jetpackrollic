﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inducator : MonoBehaviour
{
    public Slider catcher;
    public GameObject[] floors;
    public float timer = -10f;
    public CanvasGroup inducatorCG;
    public Animator witchAnim;
    public Animator broomAnim;
    public ParticleSystem magicCircle;
    public ParticleSystem magicBlast;
    private bool increasing;
    private bool decreasing;
    private bool isPressed;
    private bool isInducatorVanished;
    private float time = 1f;
    //indukatorun hızını belirleyen çarpan
    private float indukatorSpeed = 30f;

    void Update()
    {
        InducatorSlider();
        //tuşa bastığımızda inducatordan alınan oranı player classında hızı ayarlamak için yönlendiriyoruz.
        if (Input.GetMouseButtonDown(0) && !isPressed)
        {
            magicCircle.gameObject.SetActive(false);
            float tempRatio = Mathf.Abs((int) catcher.value);
            if (tempRatio <= 8)
            {
                witchAnim.SetBool("MidAir", true);
                broomAnim.SetBool("BroomMidAir", true);
            }
            floorSelector((int) catcher.value);
            magicBlast.gameObject.SetActive(true);
            //player.ratio = (int)catcher.value;
            isPressed = true;
            isInducatorVanished = true;
        }
        if (!isPressed)
        {
            catcher.value = timer;
        }

        if (isInducatorVanished)
        {
            InducatorVanisher();
        }
    }

    //indukator sürekli sağa sola kayması için gereken işlemi yapan metod
    void InducatorSlider()
    {
        if (increasing)
        {
            timer += Time.deltaTime*indukatorSpeed;

        }
        if (decreasing)
        {
            timer -= Time.deltaTime*indukatorSpeed;
        }
        if (timer <= -10f)
        {
            increasing = true;
            decreasing = false;
        }

        if (timer >= 10f)
        {
            increasing = false;
            decreasing = true;
        }
    }

    //inducatorun kaybolmasını delaylemes için kullanıyoruz.
    private void InducatorVanisher()
    {
        time -= Time.deltaTime;
        if (time < 0)
        {
            inducatorCG.alpha = 0;
        }
    }
    
    public void floorSelector(int ratio)
    {
        int tempRatio = Mathf.Abs(ratio);
        if (tempRatio <= 1)
        {
            floors[2].SetActive(true);
        }
        else if (tempRatio <=5f)
        {
            floors[1].SetActive(true);
        }
        else if (tempRatio <= 8)
        {
            floors[0].SetActive(true);
        }
        else
        {
            Debug.Log( " bişeye basamadın");
        }
    }

}
