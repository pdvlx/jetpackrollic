﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;

public class ObstacleSpawner : MonoBehaviour
{
    public GameObject obstaclesGO;
    public float distanceBetweenObstacles;
    private float startingSpawnValue = -4.0f;
    private float endingSpawnValue = 4.9f;
    
    void Start()
    {
        float tempValue = startingSpawnValue;
        bool whileChecker = true;
        while (whileChecker)
        {
            tempValue += distanceBetweenObstacles;
            GameObject tempGO = Instantiate(obstaclesGO, transform,false);
            tempGO.transform.localPosition = new UnityEngine.Vector3(0,0,tempValue);
            if (tempValue >= endingSpawnValue)
            {
                whileChecker = false;
            }
        }
    }

}
